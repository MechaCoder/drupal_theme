var gulp = require('gulp');
var shell = require('gulp-shell');
var gulp_notify = require('gulp-notify');
var sass = require('gulp-sass');



gulp.task('default', function(){

})

gulp.task('drush:cc', function(){
    return gulp.src('', {
        read: false
    })
    .pipe(shell(['drush cc views']))
    .pipe(gulp_notify("Cache has been deleted"));
})

gulp.task('sass', function(){
    return gulp.src('./css/src/**/*.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest('./css/dist/'))
});

gulp.task('watch', function(){
    gulp.watch(['./css/src/**/*.scss'], ['sass', 'drush:cc']);
    gulp.watch(['./templates/**/*.twig'], ['drush:cc']);
})